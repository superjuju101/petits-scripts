# -*-coding:Utf-8 -*

"""
Functions of chrono program (chrono.py).
"""

import time
import os
import pickle
import math

def stomh(s):
	"""
	Stomh : seconds to minutes and hours
	Transform a number of seconds in "hours, minutes, secondes". Return a tupple (h,m,s).
	"""
	m = 0
	h = 0
	while s > 59: #As long as there are more than 59 secondes...
		m+=1 #Add 1 min.
		s-=60 #Substract 60 s.
		
	while m > 59: #As long as there are more than  59 minutes...
		h+=1 #Add 1 hour.
		m-=60 #Substract 60 m.
	
	return (h,m,s)

def start_chrono():
	"""
	Run the chrono.
	"""
	current_time=time.time() #Get current numer of seconds since Epoch in a tupple.
	local_time= time.localtime(current_time) #Create a localtime copy...
	print("\nChrono lancé (à {} h {} min et {} sec)." .format(local_time.tm_hour, local_time.tm_min, local_time.tm_sec)) #... in order to print it.
	#If 'temps.data' exists, delete it.
	try:
		os.remove("temps.data")
	except FileNotFoundError:
		pass
	#On enregistre l'heure en secondes (float) dans 'temps.data'
	with open("temps.data", "wb") as fichier:
		pickle.Pickler(fichier).dump(current_time)
	print("")
	
def stop_chrono():
	"""
	Stop the chrono and print the result.
	"""
	temps_fin = time.time() #Get current time in a float.
	print("\nChrono arrêté.")
	time.sleep(0.5)
	#Get starting time from 'temps.data'
	with open("temps.data", "rb") as fichier:
		temps_depart= pickle.Unpickler(fichier).load()
	
	duree = temps_fin - temps_depart #Calculating the duration.
	duree = int(round(duree, 0)) #Rounds to unit, then transform in an int.
	
	(h, m, s) = stomh(duree) #Transform duree in seconds in a tuple (h, m, s).
	#Printing.
	if h == 0 and m == 0:
		if s <= 1:
			print("{} seconde" .format(s))
		elif s <= 59: # 2-60 sec.
			print("{} secondes" .format(s))
	elif h == 0:
		print("{} min et {} sec" .format(m, s))
	else:
		print("{} h, {} min et {} sec" .format(h, m, s))

	print("")
	
def timer():
	"""
	Allow to set an alarm according to a number of minutes and seconds.
	"""
	print("\nMinuteur")
	try:
		nbMinutes = input("Durée (minutes:secondes) du minuteur : ")
		m, s = nbMinutes.split(":")
		m, s = int(m), int(s)
		nbSecondes = (m * 60) + s
		tempsDebut = time.time() #Get current number of seconds since Epoch.
		fin = False
		while fin == False:
			time.sleep(1)
			tempsActuel = time.time()
			if tempsDebut + nbSecondes <= tempsActuel:
				fin = True
			else:
				pass
	
		print("\nTemps écoulé.\n")
	except ValueError:
		print("- erreur de format -\n")
		pass

def alarm():
	print("\nAlarme")
	try:
		hm = str(input("Horaire de l'alarme (heure:minutes) : "))
		hm = hm.replace(",", ":").replace(".", ":").replace("h",":").replace(" ", "") #Remplace certains caractères erronés.
		h, m = hm.split(":")
		h,m = int(h), int(m)
		
		tempsActuel = time.localtime(time.time()) #Récupération heure actuelle
		hDepart, mDepart = tempsActuel.tm_hour, tempsActuel.tm_min #Enregistrement heure de départ et minutes de départ
		
		#Calcul du nombre d'heures et de minutes de différence.
		hTotal, mTotal = 0,0
		if h - hDepart >= 0:
			hTotal = h - hDepart
		elif h - hDepart < 0: #Si le résultat est négatif (ex : h=14, hDepart=18 / hTotal=14 h - 18 h = -4)... 
			hTotal = 24 + (h - hDepart) #...on prend 24 et on soustrait le résultat pour avoir la différence. Donc 24 - 4 = 20 (il y a bien 18 heures entre lundi 18 h et mardi 14 h). Et on écrit ça "24 + (-4)" ici.
		
		if m - mDepart >= 0:
			mTotal = m - mDepart
		elif m - mDepart < 0: #Même raisonnement que ci-dessus mais avec 60 (nb de minutes dans une heure) au lieu de 24.
			mTotal = 60 + (m - mDepart)
		
		delaiSecondes = (hTotal * 3600) + (mTotal * 60) #On calcule le nombre de secondes équivalant au nombre d'heures et minutes d'intervalle.
		
		time.sleep(delaiSecondes-60) #Pause pendant tout le temps nécessaire - 1 minute.
		#Une minute avant, on active une boucle qui va vérifier quand ce sera l'heure EXACTE, afin de lancer l'alarme dès le début de la minute demandée, et non au cours de la minute demandée. Q° de précision.
		#Néanmoins, tout ce qui précède permet de ne pas faire fonctionner la boucle suivante en permanence. Q° d'économie de ressources.
		fin = False
		while fin is False:
			time.sleep(0.5) #Checks (ie run the loop) every 1/2 seconds. So the clock has a precision about 1/2 seconds.
			tempsActuel = time.localtime(time.time())
			hActuel, mActuel = tempsActuel.tm_hour, tempsActuel.tm_min
			if hActuel == h and mActuel == m: #If hour (en fait, elle coïncide forcément) and minutes are the same, it's time!
				fin = True
				
		if m < 10: #Si < 10 minutes, on ajoute un "0" devant le nb de minutes pour l'esthétique (avoir "2 h 05" et non "2 h 5")
			m = str(m)
			m = "0"+m
		print("Biiip. Il est {} h {}.\n" .format(h, m))
	
	except ValueError:
		print("- erreur de format -\n")
		pass