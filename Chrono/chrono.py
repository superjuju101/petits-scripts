#!/usr/bin/python3.6
# -*-coding:Utf-8 -*

"""
Author: Jules78120
Date: 16 May 2016
Licence: GNU GPL v3
Description : Program with 3 fonctionnalities: chrono, timer, and alarm.
"""

import time
import os
import pickle
import math

from functions import *


print("CHRONOMETRE")
print("\"exit\" to exit the program.\n\"start\" to start the chrono.\n\"stop\" to stop the chrono.\n\"alarm\" to program an alarm.\n\"timer\" to program a timer.\n")
command = str()
while command != "exit":
	try:
		command = str(input(">>>>>> "))
		if command == "start":
			start_chrono()
		elif command == "stop":
			stop_chrono()
		elif command == "alarme" or command == "alarm":
			alarm()
		elif command == "timer":
			timer()
		elif command == "exit":
			continue
		else:
			pass
	except TypeError:
		continue
		
print("\nGoodbye")
time.sleep(1)