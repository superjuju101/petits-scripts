# -*-coding:Utf-8 -*

"""
Author: Jules78120
Date: 16 April 2017
Licence: GNU GPL v3
Description: Tentative de recodage en python de la méthode list.insert().
"""

import os
import math

def insert_v(liste, value, index):
	"""
	Insert 'value' in 'index' of the 'liste' and returns the new list.
	"""
	continuer = True
	i = index
	while continuer is True:
		try:
			if i == index: #Pour la première valeur de i ('index' donc), il faut insérer la nouvelle valeur ('value').
				buffer1 = liste[i] #Mais avant, sauvegarder la valeur qui se trouvait là avant.
				liste[i] = value
			elif i >= len(liste): #Utilisé pour le report de la dernière valeur de la liste, qui doit être attribué à un index inexistant.
				liste += [buffer1] #Création du nouvel index et attribution de la valeur.
				continuer = False #Forcément la dernière valeur, donc on peut sortir de la boucle.
			else: #Cas général : attribue la valeur précédemment mémorisée ('buffer1') à son nouvel index ('i').
				buffer2 = liste[i] #Mais avant, sauvegarde (dans 'buffer2') la valeur qui se trouvait là avant.
				liste[i] = buffer1 
				buffer1 = buffer2
			i+=1
		except IndexError:
			print("Erreur d'index.")
			os.system("pause")
			continuer = False
	return liste

#Exemple d'utilisation (manque le numéro '6' dans la liste, qu'on demande d'ajouter avant la valeur '7', donc avant l'index '5').
if __name__ == "__main__":
	liste = [1,2,3,4,5,7,8,9,10,11,12]
	print("Ancienne liste : {}" .format(liste))
	liste = insert_v(liste, 6, 5)
	print("Nouvelle liste : {}" .format(liste))
	os.system("pause")