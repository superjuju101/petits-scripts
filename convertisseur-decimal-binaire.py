# -*-coding:Utf-8 -*

"""
Author: Jules78120
Date: 15 April 2017
Licence: GNU GPL v3
Description: Convertisseur de nombres décimaux en nombres binaires et vice-versa.
"""

import os
import math
import time

def d_to_b():
	"""
	Convertit un nombre décimal (choisi par l'utilisateur) en nombre binaire.
	"""
	sortir = False
	while sortir is False:
		print("\n--- conversion décimal -> binaire ---\nIndiquez le nombre décimal à convertir ou écrivez \"exit\" pour revenir au menu.")
		n_dec = n_depart = input(">>> ")
		if n_dec == "exit":
			print("")
			sortir = True
			continue
			
		try:
			n_dec = int(n_dec)
		except ValueError:
			print("Erreur. Les nombres à virgule ne sont pas acceptés.\n")
			continue

		#On cherche combien (i) de puissances de 2 (2^i) on peut caser dans le nombre décimal.
		i = 0
		while math.pow(2,i) <= n_dec:
			i+=1

		#Pour chaque valeur de i, de manière décroissante on cherche si 2^i est compris dans le nombre décimal.
		resultat = 0
		while i >= 0:
			if n_dec >= math.pow(2,i): #Si oui, alors la conversion binaire de ce 2^i est un 1
				chiffre_courant = "1"
				n_dec -= math.pow(2,i)
			else: #Si non, la conversion binaire de ce 2^i est un 0
				chiffre_courant = "0"
			resultat = int(str(resultat) + chiffre_courant)
			i-=1

		print("\n{} (déc.) = {} (binaire)" .format(n_depart, resultat))

def b_to_d():
	"""
	Convertit un nombre binaire (choisi par l'utilisateur) en nombre décimal.
	"""
	sortir = False
	while sortir is False:
		print("\n--- conversion binaire -> décimal ---\nIndiquez le nombre binaire à convertir ou écrivez \"exit\" pour revenir au menu.")
		n_bin = n_depart = input(">>> ")
		if n_bin == "exit":
			print("")
			sortir = True
			continue
		
		resultat = 0
		i = len(n_bin)-1
		for chiffre in n_bin:
			multiplicateur = int(math.pow(2,i))
			resultat += int(chiffre) * multiplicateur
			i-=1

		print("\n{} (binaire) = {} (déc.)" .format(n_depart, resultat))

#CODE PRINCIPAL		
print("=== CONVERTISSEUR DECIMAL - BINAIRE ===")
sortir = False
while sortir is False:
	print("Tapez le numéro de la fonction désirée.\n1. Convertisseur décimal -> binaire\n2. Convertisseur binaire -> décimal\n3. Fermer le programme")
	choix = input(">>> ")
	if choix == "1":
		d_to_b()
	elif choix == "2":
		b_to_d()
	elif choix == "3":
		print("Au revoir.")
		time.sleep(1)
		exit()
	else:
		print("")
		continue